import {
  WebGLRenderer,
  Scene,
  PerspectiveCamera,
  Color,
  BoxGeometry,
  MeshBasicMaterial,
  Mesh
} from '/three/build/three.module.js'
import Stats from '/three/tools/jsm/libs/stats.module.js'

const renderer = new WebGLRenderer({ antialias: true })
renderer.setSize(window.innerWidth, window.innerHeight)
// Show Stats
const stats = new Stats()

document.body.appendChild(renderer.domElement)
document.body.appendChild(stats.dom)

// Create Scene
const scene = new Scene()
scene.background = new Color(0x202020)

const updateFrame = () => {
  requestAnimationFrame()
}
